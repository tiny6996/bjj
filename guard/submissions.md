# submissions from guard
## Olma plata

### setups
* traditional armbar
  *  grab one arm cross and pull it over push the head away
  * go for the armbar and he pulls it out
  * grab the belt or  pant to help pull your hips through on the olma plata
    * position your legs in the air not when they are on the ground
* slick your air back
  * grab cross wrist take it behind the head and swim your hand into an overhook
  * push off the far hip with our foot
  * switch your hands
  * grab the belt and sit up

### finishes
* wrestling cheerleader


## cross choke from guard
* break posture by doing with your hands on their elbows or rope climb and pull down
* keep that hand under
* take your hand under your arm and grab the other lappel palm down
* pull elbows to the matt
* if you have trouble break guard place your feet inside his and spread his legs
* or place your feet on his hips and pull yourself up and straighten your legs
* if you can't get your hand under you can posistion your forearm in his neck and grab the spot behind the ear the wrinkle
* or swish hips to the side you can't get your grip then place forearm and grab the spot behind the ear or the wrinkle


### loop choke from gaurd
* get a shallow cross grip
* take your other hand across the hand around the head
* open your guard and slide for hips
* take the hand over the head and push out.
  * keep a strong wrist by making a fist or waving

### loop choke from half gaurd or half butterfly
* get a shallow cross grip and hand over the head
* move your hips out and clamp your leg on their lat so they can't esscape
* push the neck down hips away
* if your grips go switch to mono plata

## their arm crosses the center line
* instead of posture break pull them up and on you
* grab cross wrist and v grip the button of the tricep
* pull across

### cool choke
* instead of the far lat fish for a thumb out grip on the far lapel
* clamp your elbow they will either tap or roll
* if they roll grab the opposite lapel or wrinkle and finish like a cross choke

### arm bar if you can't get an angle
* slide under them similar to a deep half
* place your arm over their arm and under their leg
* take your free hand and push the head
* take your leg and place it over their head
* arch very slowly

## Butterfly guard
* place both feet inside on their inner thighs
* either try to control under hooks or an arm drag position
* this is a good place to go when recovering guard from side mount especially if you are having issues getting closed guard
### arm drag position
* grab the tricep crossbody and the wrist
  * in gi you can grab your own lapel instead of their tricep
* pull in and up as much as possible to prevent their arm from escaping
* do not transition to grabbing their lat right away this gives away your back and limits your ability to other moves
### loop choke from butterfly or closed guard
1. get a cross collar grip
2. take your other hand around the head and pull it down
3. take the blade of the cross grip across their throat and take the hand around the head and pinch it down

### butterfly to heel hook from arm drag
* do this if the push in and try and pass you after you get your armdrag
1. transition to a modified x position by...
2. push their weight to their farside knee
3. push them back and squeeze your knees to keep their knee from getting above your knees
4. grab over their heel and pull it towards you or fish for the heel like a regular heel hook


## triangles (illumanati confrimed)
you will always need the following things to complete a triangle. 
There are many entries but the finish is usually the same.
When in doubt keep their posture broken 
* break posture
* bridge your hips/stiff as a board
* shove their arm across 
* change your angle 
* toes to the sky 

### one in one out 
* push one arm and pull the other while you turn hips so your butt is torwads the pulled hand 
* repeat until you can take your leg around the pushed side 
* get to the starter triangle
* finish

### alternative finishes

if you are having trouble with a regular finish. 
you can get your arm in and woke a cross choke. 

* if they have a gap and/or move their arm back across the centerline 
  * grab the crown of the head and til the chin into your thigh
  * reach around the head and pull down (trucker elbow )

* if you get ther arm across the centerline 
  * do a crunch
  * reach both arms over to lock on their elbow 
  * pull the elbow down and turn it over 

* if they posture up 
  * open your triangle briefly so you can slide it down and relock
  * grab a cross collar break their posture 
  * finish the triangle or switch it to a collar choke 

* if they posture up (armbar)
  * keep your triangle and striaghten the arm 
  * grab the meaty part of the hand that is in and turn it palm up 
  * push hips up to finish the armbar

### flower sweep 
* go for a flower sweep and if they post 
* slide your foot out from the posted arm and place it behind their neck 
* finish 
* if they don't scoop you can kick the high leg out and place the leg behind the head 


### scissor sweep 
* go for a scissor sweep 
* if they try and scoop then then free your leg and put it behind the head. 
* lock your other triangle 
* work the finish 


### punches 
* they throw a punch from guard block one with your shin and for arm 
* block the other hand when they go the other punch block their bicept with your foot
* take the foot blocking the bicep and take it across the back of their head 
* lock their triangle

### over hook 
* break their posture 
* get the overhook 
* get a c grip on the wrist and shrimp so your butt moves towards the underhook 
* slide your leg out and place it across his neck 
* get the traingale  

### arm over sweep 
* get a cross collar grip 
* get your hips up and post your other hand like an arm over sweep 
* extend your hand in the collar like a punch 
* fall back and get slide your leg across the back of the neck
* and lock them up 
