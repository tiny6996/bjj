# sweeps
## open sweeps

### flower
* start sleeve collar and they start to break
* get z gaurd and get a foot on both hips
*

### switch

### tomonagi from guard
* get sleeve collar
* get both feet on the sexlines
* push pull until their balance breaks then pull them stright over or cross body

## flower sweep

### step up
* get a cats paw grip and get a sticky eblow so they can't free their hand
* if they step up slide under and shoot your under their thigh
* disco move move your free leg and push with your trapped leg
* pull your free leg under to help you finish in mount


### no step up
* same as regular but you have to get them learning forward
* you can start sleeve collar to get their weight forward

### transistion to triangle
* start the flower sweep
* if they post out you can shoot your leg under if you stay on your hip
* go for triangle

### transition to armbar
* start flower sweep
* pull their hand through
* straighten your leg and pass it around the head
* bite down over the head and pull the head
### gateway sweep
* grab the hand the same way
* grab their pants
* start going pull the leg and use the same hip and leg motion as flower sweep

## scissor sweeps

### regular
 * get sleeve collar on the same side
 * get knee shield on the opposite side as your grips
  * knee is almost at 90
 * pull in the sleeve and collar and don't let them smash your knee
 * once you feel their weight lift place your leg at their knee and reap
 * push your collar grip to the mat
### double cat paw
* both hands cat paw one hand and pull
* transistion to knee shield and create space with your free foot
* make sure your are on the knee push out the leg instead of reaping
### push sweep
* same finish as cat paw but have sleeve collar
### flippide do
* try scissor sweep and fail
* get two cat paws on a sleeve
* change your knees so your opposite knee is the knee shield
* grab lat your knee shield is on
* plant your other foot on the mat
* pull up and place them on their side l


## crossing the center line
* instead of posture break pull them up and on you
* grab cross wrist and v grip the button of the tricep
* pull across

### easy mode
* once you get them across the center line
* keep the wrist
* grab the far lat behind there back
* open your guard and twist your hips
* post on your forearm
* get seat belt and pull them into a back take

### if you can't post your elbow
* pull them in
* stomp the mat with the foot that is on the same side as the hand that is grabbing the lat
* shrimp and pull your other foot over


### if you can get gift wrap
do everything the same but instead of having a hand on the lat gift wrap the head and sticky elbow when you sweeps

## Butterfly guard
* place both feet inside on their inner thighs
* either try to control under hooks or an arm drag position
* this is a good place to go when recovering guard from side mount especially if you are having issues getting closed guard

### arm drag position
* grab the tricep crossbody and the wrist
  * in gi you can grab your own lapel instead of their tricep
* pull in and up as much as possible to prevent their arm from escaping
* do not transition to grabbing their lat right away this gives away your back and limits your ability to other moves

### pummel clinch
* either one underhook and one overhook or double unders
### back take from arm drag
1. get the arm position
2. shrimp your hips towards the arm you control
3. move the hand on their wrist to their farside lat
4. drag the arm across and slide your hook in

### scissor sweep from arm drag
1. get the arm drag
2. load them up pulling the arm into your chest and leaning back and push or they try and pin you down
3. push your legs out and drag the arm
4. reap the foot on the same side as the arm you control if they try and plant their foot
  * the lower you are their foot the better since you have more leverage
5. keep the arm and transition to side mount or full mount
### gravity back take  from arm drag
1. get the armdrag position
2. load them up by leaning back and pushing your legs up this also works if the push into you
3. release the leg that is on the same side as the arm you control and kick the other leg right inside their thigh
4. transition to back take


### headbutt from clinch
1. get double underhooks from the pummel position
2. place your head in the center of their just
3. charge in

### sissor sweep from clinch
1. get over under position from butterfly guard
2. load them up by crunching your abs and leaning back
3. while you are leaning back push your over hook and pull your over hooks
4. slide your overhook side leg out and kick their knee or reap the outside of the knee 
