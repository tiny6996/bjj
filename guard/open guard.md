# Open guard 
Open guard is when your legs are between you and your opponent but they your legs are not wrapped around them. 
In open guard it is important to keep your hips off the ground as much as possible. 
This makes it much easier for you to move with your opponent

## everywhere sweep 
This sweep is easy to get to from most forms of open guard.
An easy way to learn is start with with both placed on your opponents hips 
* get sleeve collar grip on one side 
* place the opposite foot on the back of their leg 
* take the hand on their sleeve and grab their heel 
* pull them in and lift them up 
* push forward and place them on the mat 


### transition to x guard 
* if they start spread out wide
  * crunch your body towards they leg you grabbed 
  * twist your legs into X guard 


### reverse it
do only if your opponent bases out. form regular open guard it is hard to move your legs around and complete the reap 
* take the foot that is placed on the back of the leg and move it to the opposite hip 
* take the other foot and place it behind their leg 
* with a slight bend in the knee move it back think of a sythe reaping wheat

## arm drag
* get a cross grip on the wrist and pull it across the center line 
* take your other arm and grab the tricep and suck it in 

### keys 
* if you want access to the back at all their should has to be below your shoulder 
* you want keep the arm close to your trunk at all times so you have controll

### kick the far leg 
* starts from kneeling 
* the leg on the same side of the dragged arm is one the outside and the other leg is on the knee
* pull the arm and kick the far knee out and transition to back mount 

### regular back take 
* start from butterfly or both legs on the knees 
* get the arm drag and pull down and over 
* if their shoulder is exposed take the leg opposite of the dragged arm and slide it in 
* scoot your hips around their back this should result with back mount with one hook in 

### if they step up
* if you get the arm drag and start to pull in they might step up 
* you can slide your leg under theirs
* let go of the wrist and take around and through their leg 
* pull them in and roll them over you and you can follow them into mount or curly run around to north south 
* if they push down on your knee when they step up 
* shoot your legs into single leg X 

## Dela le riva 
* take one foot and wrap the near leg outside in and flex your foot on their inner thigh 
* get a cats paw grip on the pant leg on the same side you have hooked with the same side hand 
* get a cross collar grip on the same side 
* push your other leg on their far knee and try to get perpendicular 

### hook sweep 
* a perferred setup is when they try breaking the collar grip and posturing up but this can work from regular de la riva 
* take the foot pushing on the knee and hook the back of the far leg 
* remove your de la riva hook and place it on the hip so your toes are on their hip bone 
* slide the hook on the far leg down to the ankle and push forward with your foot 
* switch hands for the grip that is still on their pant and do a technical stand up

### back take 
* take the hand that is on their pants and hug their knee 
* grab their belt with your other hand 
* shrimp around their back until they are forced to back step
* get a pistol grip on the far pant leg 
* slide your free leg behind their near thigh then release your de la riva hook place the top of your foot on the back of their thigh 
* adjust the grip on their belt so it is on their spine 
* kick your legs out until their balance breaks then pull the belt 
* get a seatbelt grip getting the underhook first and choking hand second then get your hooks in 