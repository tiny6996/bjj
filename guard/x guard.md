# X guard 

## single leg X 
from your back take you have one leg wraped around their leg and you using the other leg to bite the other leg or control the far leg

### spear sweep 
* take the hand that is on  the same side as the wrapped leg 
* squeze their leg with both your legs and shoot your hips in the air
* turn your hips towards their back and transition to combat base 


### stand up sweep 
* do if your opponent is leaning forward 
* push on the far knee with your low leg 
* keep the grip on the near leg and do a technical get up and it should look like a single leg finish



## full X 

### entry from single X 
* works well  if your opponent leans forward after you setup single X 
* take leg snaked around the near leg and keep between their legs with your foot on the inside of their upper thigh 
* take the other leg and keep it behinde then 

### entry from butterfly guard
* use any dominant grip to pull them in
    * wrist collar 
    * arm drag
    * reverse arm drag
* slide legs to the side with the dominant grip 
* extend your legs upward 
* once your opponent is standing up switch your legs into x guard 

### shin to shin to single leg X 
* if your opponent steps into you sneak your shin between their leg and your body 
* grab their leg with the same arm as the shin like a single 
* roll back and push up with your shin
* pop up into single leg X 
### forward sweep
* if the lean forward grab the far arm and pull it in 
* kick your x guard straight up 
* once they start tipping get your legs back and roll into side mount 

### backward sweep 
* if they stand striaght up or lean back extend your guard and push them back with your legs 