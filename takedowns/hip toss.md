# takedowns
## uki goshi (small hip toss)

## oh goshi (big hip toss)

## herai goshi (hip toss with reap)

## hani makikomi (headlock)
### actual lock
* lock underhook and head
* step in and hip toss

### judo style
* get collar tie or lapel grip and cats paw or wrist
* pull up the cats paw and step in
  * wipe your nose with their wrist.
* hip toss

### pummel
* do from over under Standing
* when the pummel for your underhook clamp your overhook
* while clamping step in
* hip toss

### counter uki goshi with drop seogi
* if someone goes for uki goshi in the clinch step around
* then go for a deep drop seogi

## ippon seogi

### judo setup
* grab seam of their sleeve
* pull it up and in (try and wipe your nose with their sleeve)
### make them set themselves up
* push into them with your your hand on the seam of their sleeve
* when they step into you pull them in
### finish for both
* lower your level while backsteping into their arm and place the arm on the outside of your shoulder
* extend your legs and touch your ear to your opposite knee

## armsipin

## standing guilotine counter 
* first thing create an airway by grabbing the forarm and pull down with the hand closest to the choke

* grab the over the other shoulder
* step around towards the shoulder and create a shelf with the leg behind his 
* look away like someone is trying call your name

## if they trap your arms 
* push off the hips and step back 
* underhook the side opposite your head 
* step arcoss so your heel is right in front of your toe 
* squat and pull them up and over extend your knees to get a good throw 
### to stop them while they are coming in
* keep your hips back and shove your hands in their face 
* if they get double unders you can do this is but you gave up legs so don't do this to someone who knows takedowns 